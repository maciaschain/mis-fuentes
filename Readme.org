#+TITLE: mis-fuentes

* Descripción

Una extensión para Helm que permite listar las fuentes instaladas en el sistema y aplicar una serie de acciones sobre el
candidato que tiene el foco.

Podemos aplicar, de momento, cuantro acciones (la lista de acciones se obtiene pulsando tabulador):

- Insertar el nombre de la fuente en el punto del cursor (acción por defecto, con enter)
- Definir una nueva familia para el candidato escogido, con la sintaxis de fontspec
- Definir una nueva familia para el candidato escogido, con la sintaxis de babel
- Visitar el directorio del candidato escogido


* Instalación y uso

Requiere tener instalado Helm. Guardar el archivo =helm-mis-fuentes.el= en cualquier directorio accesible a Emacs. Y
añadir al archivo de inicio:

#+begin_src emacs-lisp
(load "helm-mis-fuentes.el")
#+end_src

Para activarlo, llamar a la función =M-x helm-mis-fuentes= o asignarle un atajo de teclado.

* Un vídeo de ejemplo

https://vimeo.com/424387030
