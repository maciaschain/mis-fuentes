;;; helm-mis-fuentes.el

;; Copyright (C) 2020  Juan Manuel Macías

;; Author: Juan Manuel Macías <maciaschain@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:


(require 'helm)

;; Para acceder a la carpeta de las fuentes
;; esta variable nos lista toda la info de las fuentes del sistema

(setq fuentes-sistema (shell-command-to-string "convert -list font"))

(defun visita-directorio-fuente (fuente)
  "visita el directorio de una fuente"
  (interactive)
  (with-temp-buffer
    (insert fuentes-sistema)
    (goto-char (point-min))
    (while (re-search-forward (concat "family: " fuente) nil t))
    (re-search-forward "glyphs: " nil t)
    (set-mark-command nil)
    (end-of-line)
    (copy-region-as-kill (region-beginning) (region-end))
    (deactivate-mark))
  (dired
   (file-name-directory (car kill-ring)))
  (re-search-forward (file-name-sans-extension (car kill-ring)) nil t))

;; Y aquí viene el código propio de Helm.

;; La primera función genera la lista de candidatos. Uso la función =font-family-list=.

(defun candidatos-fuente ()
  "Devuelve una lista con las fuentes instaladas en el sistema"
  (font-family-list))

;; Lista de acciones. La acción por defecto es simplemente insertar el nombre de la familia
;; de la fuente en el punto del cursor.

(setq lista-fuentes-helm-acciones
      (helm-make-actions
       "Insertar familia de fuentes" (lambda (fuente)
				       (insert fuente))
       "Definir nueva familia para Fontspec" (lambda (fuente)
					       (let
						   ((familia (read-from-minibuffer "Nombre: "))
						    (propiedades (read-from-minibuffer "propiedades (enter para dejar en blanco): ")))
						 (insert (concat "\\newfontfamily\\" familia "{" fuente "}" "[" propiedades "]"))))
       "Definir nueva familia para Babel" (lambda (fuente)
					    (let
						((lengua (read-from-minibuffer "lengua (enter para dejar en blanco): "))
						 (familia (read-from-minibuffer "familia (rm, etc. Enter para dejar en blanco): "))
						 (propiedades (read-from-minibuffer "propiedades (enter para dejar en blanco): ")))
					      (insert (concat "\\babelfont" "[" lengua "]" "{" familia "}" "[" propiedades "]" "{" fuente "}"))))
       "Visitar el directorio de la fuente" (lambda (fuente)
					      (visita-directorio-fuente fuente))))


;; Y, por último, la variable para esta extensión, y la función que lanza Helm con esa variable.

(setq lista-fuentes-helm
      '((name . "Lista de fuentes")
	(candidates . candidatos-fuente)
	(action . lista-fuentes-helm-acciones)))

(defun helm-mis-fuentes ()
  (interactive)
  (helm :sources '(lista-fuentes-helm)))

;;; helm-mis-fuentes.el ends here
